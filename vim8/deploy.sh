#!/usr/bin/env bash
BASEDIR=$(realpath $(dirname $0))

# deploy vimrc & .vim
RC="$HOME/.vimrc"
VIMDIR="$HOME/.vim"
if [ -e $RC ] || [ -e $VIMDIR ]; then
    echo "$RC / $VIMDIR already exists"
    exit 1
else
    ln -s $BASEDIR/vimrc $RC
    ln -s $BASEDIR/vim $VIMDIR
fi
echo -e "\n\nYou may want to run ':PlugInstall' or ':PlugUpdate' in vim\nSome plugins (e.g. YouCompletesMe) may need to be installed manually"

