## Install
```sh
sudo apt install tightvncserver
sudo apt install xfce4 xfce4-goodies
ln -s $PWD ~/.vnc
```

## Start Server
`vncserver -localhost -geometry 1920x1080`

## Client
vnc://localhost:5901

## Kill Server
`vncserver -kill :1`

