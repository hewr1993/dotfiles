mkdir /etc/systemd/system/docker.service.d
ln -s $PWD/http-proxy.conf /etc/systemd/system/docker.service.d/http-proxy.conf

or

mkdir ~/.docker
ln -s $PWD/config.json ~/.docker
