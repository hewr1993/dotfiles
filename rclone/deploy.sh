#!/bin/bash
EXECDIR=$PWD
BASEDIR=$(realpath $(dirname $0))

set -x
[ ! -r $HOME/.config/rclone ] && ln -s $BASEDIR $HOME/.config/rclone

