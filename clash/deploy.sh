#!/usr/bin/env bash
mkdir -p $HOME/.config/clash
ln -s $PWD/config.yaml $HOME/.config/clash
ln -s $PWD/proxyIgnoreList.plist $HOME/.config/clash
tar xf yacd.tgz
ln -s $PWD/yacd $HOME/.config/clash
