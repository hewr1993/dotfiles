#!/usr/bin/env bash
BASEDIR=$(realpath $(dirname -- $0))
#sudo apt-get install -y libtool-bin
pushd neovim
make CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=$BASEDIR/../bin/neovim"
make install
popd

ln -s $PWD/nvim-config $HOME/.config/nvim

#sudo apt-get install -y ccls bear

# `bear make -j8` to generate `compile_commands.json` for ccls

# https://github.com/Karmenzind/monaco-nerd-fonts
