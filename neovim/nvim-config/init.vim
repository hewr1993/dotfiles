let g:python_host_prog='/usr/bin/python3'

set nocompatible
filetype off

call plug#begin("~/.config/nvim/bundle")
" Plugin List
Plug 'skywind3000/quickmenu.vim'
Plug 'editorconfig/editorconfig-vim'

"Plug 'bigeagle/molokai'
Plug 'overcache/NeoSolarized'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'Yggdroot/indentLine'
Plug 'tmhedberg/simpylfold'
"Plug 'Valloric/MatchTagAlways'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

Plug 'mileszs/ack.vim'
Plug 'Shougo/echodoc'
Plug 'majutsushi/tagbar'
Plug 'jrosiek/vim-mark'  "<leader>M 多个高亮单词
Plug 'kien/rainbow_parentheses.vim'
Plug 'honza/vim-snippets'
Plug 'airblade/vim-gitgutter', {'branch': 'main'}

Plug 'scrooloose/nerdcommenter'
Plug 'mattn/emmet-vim'  "html语法补全
Plug 'hdima/python-syntax'
Plug 'hynek/vim-python-pep8-indent'
Plug 'jason0x43/vim-js-indent'
Plug 'zaiste/tmux.vim'
Plug 'lepture/vim-jinja'
Plug 'cespare/vim-toml', {'branch': 'main'}
Plug 'othree/html5.vim'
Plug 'ekalinin/Dockerfile.vim'
Plug 'chiphogg/vim-prototxt'
Plug 'machakann/vim-highlightedyank'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'ntpeters/vim-better-whitespace'
Plug 'chrisbra/csv.vim'
Plug 'zchee/vim-flatbuffers'

Plug 'neoclide/coc.nvim', {'do': 'npm install --frozen-lockfile'}
Plug 'neoclide/coc-json', {'do': 'npm install --frozen-lockfile'}
"Plug 'neoclide/coc-python', {'do': 'npm install --frozen-lockfile'}
Plug 'fannheyward/coc-pyright', {'do': 'npm install --frozen-lockfile'}
Plug 'neoclide/coc-yaml', {'do': 'npm install --frozen-lockfile'}
Plug 'neoclide/coc-highlight', {'do': 'npm install --frozen-lockfile'}
Plug 'neoclide/coc-lists', {'do': 'npm install --frozen-lockfile'}
Plug 'neoclide/coc-snippets', {'do': 'npm install --frozen-lockfile'}
Plug 'neoclide/coc-git', {'do': 'npm install --frozen-lockfile'}
"Plug 'neoclide/coc-tabnine', {'do': 'npm install --frozen-lockfile'}
Plug 'neoclide/coc-tsserver', {'do': 'npm install --frozen-lockfile'}
Plug 'neoclide/coc-eslint', {'do': 'npm install --frozen-lockfile'}

Plug 'github/copilot.vim', {'on': 'Copilot', 'branch': 'main'}
Plug 'aduros/ai.vim', {'branch': 'main'}  " ctrl-a to trigger
Plug 'oelmekki/make-my-code-better.vim'

call plug#end()

" UI
if !exists("g:vimrc_loaded")
	if has("nvim")
		set termguicolors
	endif
	let g:molokai_original = 1
	"colorscheme molokai
	colorscheme NeoSolarized
endif " exists(...)

set scrolloff=0
set number
syntax on
filetype on
filetype plugin on
filetype indent on

set list lcs=tab:\¦\   

if has("autocmd")  " go back to where you exited
    autocmd BufReadPost *
        \ if line("'\"") > 0 && line ("'\"") <= line("$") |
        \   exe "normal g'\"" |
        \ endif
endif

set completeopt=longest,menu " preview

let mapleader=","  " remap leader key
set pastetoggle=<F12> "切换粘贴模式

if has('mouse')
    "set mouse=a
    "set selectmode=mouse,key
    "set nomousehide
    set mouse=c
endif

set autoindent
set modeline
set cursorline
set cursorcolumn

set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab  " tab as whitespace

autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
autocmd FileType html setlocal shiftwidth=2 tabstop=2

set showmatch
set matchtime=0
set nobackup
set nowritebackup
"set directory=/tmp/.swapfiles//
set noswapfile
set history=200                        " command line history
set viminfo=""
"set viminfo+=n$HOME/.viminfo

" solve the slow problem
set lazyredraw
set ttyfast
let g:matchparen_timeout = 2
let g:matchparen_insert_timeout = 2

"if has('nvim')
   "set ttimeout
   "set ttimeoutlen=0
"endif

"在insert模式下能用删除键进行删除
set backspace=indent,eol,start

set fenc=utf-8
set fencs=utf-8,gbk,gb18030,gb2312,cp936,usc-bom,euc-jp
set enc=utf-8

"按缩进或手动折叠
augroup vimrc
  au BufReadPre * setlocal foldmethod=indent
  au BufWinEnter * if &fdm == 'indent' | setlocal foldmethod=manual | endif
augroup END
set foldcolumn=0 "设置折叠区域的宽度
set foldlevelstart=200
set foldlevel=200  " disable auto folding
" 用空格键来开关折叠
nnoremap <space> @=((foldclosed(line('.')) < 0) ? 'zc' : 'zo')<CR>
vnoremap <Space> zf

"set autochdir
set hlsearch    "开启搜索高亮
"set nohlsearch  "关闭搜索高亮
set incsearch   "输入搜索字符串的同时进行搜索
set ignorecase  "搜索时忽略大小写
set smartcase

vmap j gj
vmap k gk
nmap j gj
nmap k gk

nmap T :tabnew<cr>

au FileType c,cpp,h,java,css,js,nginx,scala,go inoremap  <buffer>  {<CR> {<CR>}<Esc>O

au BufNewFile *.py call ScriptHeader()
au BufNewFile *.sh call ScriptHeader()

function ScriptHeader()
    if &filetype == 'python'
        let header = "#!/usr/bin/env python3"
        let cfg = "# vim: ts=4 sw=4 sts=4 expandtab"
    elseif &filetype == 'sh'
        let header = "#!/bin/bash"
    endif
    let line = getline(1)
    if line == header
        return
    endif
    normal m'
    call append(0,header)
    if &filetype == 'python'
		call append(2, "if __name__ == \"__main__\":")
		call append(3, "    pass")
		call append(4, "")
        call append(5, cfg)
    endif
    normal ''
endfunction

" --- Plugin Configs ---------
let g:tagbar_width = 30
nmap tb :TagbarToggle<cr>

let g:localvimrc_ask=0
let g:localvimrc_sandbox=0

autocmd Filetype json let g:indentLine_enabled = 0
let g:vim_json_syntax_conceal = 0
let g:markdown_syntax_conceal=0
let g:indentLine_noConcealCursor=""
"let g:indentLine_setConceal = 0


" ----------------------------

" Completion and echodoc
set shortmess+=c
set updatetime=300
set signcolumn=yes
set completeopt=noinsert,menuone,noselect

let g:coc_auto_copen=1

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <C-j> for jump to next placeholder, it's default of coc.nvim
"let g:coc_snippet_next = '<c-j>'
" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
"let g:coc_snippet_prev = '<c-k>'
" Use <C-j> for both expand and jump (make expand higher priority.)

" https://github.com/neoclide/coc.nvim/blob/e1a4ce4d95d1d89b6dd31019cc4387425aa09b86/doc/coc.txt#L863
" use tab to navigate to the next item
inoremap <silent><expr> <TAB>
  \ pumvisible() ? "\<C-n>" :
  \ <SID>check_back_space() ? "\<TAB>" :
  \ coc#refresh()
" use tab for autocompletion
"inoremap <silent><expr> <TAB>
  "\ pumvisible() ? coc#_select_confirm() :
  "\ coc#expandableOrJumpable() ?
  "\ "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
  "\ <SID>check_back_space() ? "\<TAB>" :
  "\ coc#refresh()
"let g:coc_snippet_next = '<tab>'

inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
"inoremap <silent><expr> <leader><space> coc#refresh()


"nmap <silent> <leader>g <Plug>(coc-definition)
"nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> <leader>d <Plug>(coc-declaration)
nmap <silent> <leader>g <Plug>(coc-definition)
nmap <silent> <leader>i <Plug>(coc-implementation)
nmap <silent> <leader>u <Plug>(coc-references)
nmap <silent> <leader>rn <Plug>(coc-rename)

nnoremap <silent> K :call <SID>show_documentation()<CR>
nmap <c-p> :CocList files<CR>

function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
autocmd CursorHold * silent call CocActionAsync('highlight')

highlight CocErrorSign ctermfg=215 guifg=#ffaf5f
highlight default CocHighlightText guibg=#767676 ctermbg=243 cterm=underline

"set runtimepath^=/data/storage/coc/target

" ----------------------------

" - NerdTree -----------------
"nmap nt :NERDTreeToggle<cr>
let NERDTreeShowBookmarks=0
let NERDTreeMouseMode=2

let NERDTreeWinSize=25
let NERDTreeIgnore = ['\.pyc$']
let NERDTreeMinimalUI=0
let NERDTreeDirArrows=1
" ----------------------------

" - python and jedi ----------
let python_highlight_all = 1
"autocmd BufWritePre *.py :%s/\s\+$//e  " strip whitespaces on save
au FileType python setlocal cc=80
" ----------------------------

" - rainbow_parentheses ------
let g:rbpt_colorpairs = [
	\ [158, '#00ceb3'],
	\ [081, '#00a3ff'],
	\ [214, '#ff8d00'],
	\ [123, '#3fffc9'],
	\ [045, '#29b9ec'],
	\ [190, '#bfec29'],
	\ [208, '#ffad00'],
	\ [117, '#48bde0'],
	\ ]

let g:rbpt_max = 8
let g:rbpt_loadcmd_toggle = 0

au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax c,cpp,go,h,java,python,javascript,scala,coffee RainbowParenthesesLoadSquare
au Syntax c,cpp,go,h,java,python,javascript,scala,coffee,scss  RainbowParenthesesLoadBraces
" ----------------------------

" begin - quickmenu
let g:quickmenu_options = "HL"
noremap <silent><F1> :call quickmenu#toggle(0)<cr>
call quickmenu#reset()
call quickmenu#append('# Shortcuts', '')
call quickmenu#append('CocList', 'CocList', '')
call quickmenu#append('CocConfig', 'CocConfig', 'Open coc-settings.json')
call quickmenu#append('NERDTree', 'NERDTreeToggle', 'Directory Structure')
call quickmenu#append('Git Commit', 'CocCommand git.showCommit', 'commit history of current line')
call quickmenu#append('Git Refresh', 'CocCommand git.refresh', '')
call quickmenu#append('GitGutters', 'CocCommand git.toggleGutters', 'show file changes status')
call quickmenu#append('ESLint', 'CocCommand eslint.executeAutofix', 'JS lint')
call quickmenu#append('Copilot', 'Copilot', '')
call quickmenu#append('MakeMyCodeBetter', 'MakeMyCodeBetter', '')
" end - quickmenu

" begin - editorconfig
au FileType gitcommit let b:EditorConfig_disable = 1
" end - editorconfig
"
" begin - highlightedyank
let g:highlightedyank_highlight_duration = 250
if !exists('##TextYankPost')
  map y <Plug>(highlightedyank)
endif
" end - highlightedyank

" begin - echodoc
set noshowmode
let g:echodoc_enable_at_startup = 1
" end - echodoc

" begin - cpp enhanced highlight
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_posix_standard = 1
"let g:cpp_experimental_simple_template_highlight = 1  " super slow
"let g:cpp_experimental_template_highlight = 1
let g:cpp_concepts_highlight = 1
let g:cpp_no_function_highlight = 1
" end - cpp enhanced highlight

" begin - ack
cnoreabbrev Ack Ack!
nnoremap <Leader>a :Ack!<Space>
" end - ack

" begin - simpylfold
set foldmethod=indent
set foldlevel=99
nnoremap <space> za
let g:SimpylFold_docstring_preview=1
" end - simpylfold

" begin - OpenAI
let g:open_ai_key="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"
" end - OpenAI

" Load local config if exists
if filereadable(expand("~/.config/nvim/local.vim"))
	source ~/.vim/config/local.vim
endif
