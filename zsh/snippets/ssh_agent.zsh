if ! pgrep -u $USER ssh-agent > /dev/null; then
    [ -d $HOME/.config ] || mkdir -v $HOME/.config
    ssh-agent > $HOME/.config/ssh-agent-thing
    echo "ssh-agent started"
fi
eval $(<$HOME/.config/ssh-agent-thing) > /dev/null
