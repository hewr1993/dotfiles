#!/bin/bash
EXECDIR=$PWD
BASEDIR=$(realpath $(dirname $0))

# deploy oh-my-zsh
cd $BASEDIR/oh-my-zsh
#git submodule update --init --recursive
cd $EXECDIR

$BASEDIR/host-env/deploy.sh

# deploy zshrc
RC="$HOME/.zshrc"
if [[ -e $RC ]]; then
    echo "$RC already exists"
    exit 1
fi

set -x
ln -s $BASEDIR/zshrc $RC
ln -sf $BASEDIR/zsh_history $HOME/.zsh_history
