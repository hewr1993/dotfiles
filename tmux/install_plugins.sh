#!/bin/bash
EXECDIR=$PWD
BASEDIR=$(realpath $(dirname $0))
PLUGIN_DIR=$BASEDIR/plugins

# update submodules
cd $BASEDIR/plugins
#git submodule update --init --recursive
cd $EXECDIR

mkdir -p $HOME/.tmux
ln -s $PLUGIN_DIR $HOME/.tmux/plugins

# install tmux-mem-cpu-load
cd $PLUGIN_DIR/tmux-mem-cpu-load
cmake .
make -j
INSTALL_DIR="$BASEDIR/../bin"
make DESTDIR="$INSTALL_DIR" install
cd $EXECDIR

echo "press PREFIX+I to install plugins"
