#!/bin/bash
EXECDIR=$PWD
BASEDIR=$(realpath $(dirname $0))

# deploy version conf
TMUX_VERSION=$(tmux -V | cut -c 6-)
#if (( $(echo "$TMUX_VERSION < 2.1" |bc -l) )); then
if [[ ! -e "$BASEDIR/tmux.$TMUX_VERSION.conf" ]]; then
    echo "no suitable tmux.conf for version $TMUX_VERSION"
    exit 1
else
    ln -s $BASEDIR/tmux.$TMUX_VERSION.conf $HOME/.tmux.version.conf
fi

# deploy tmux.conf
TMUX_CONF="$HOME/.tmux.conf"
if [[ -e $TMUX_CONF ]]; then
    echo "$TMUX_CONF already exists"
    exit 1
else
    ln -s $BASEDIR/tmux.conf $TMUX_CONF
fi

# deploy user script
OS=$($BASEDIR/../bin/which_os)
USER_SCRIPT="$HOME/.tmux.user.conf"
if [[ ! -e "$BASEDIR/$OS.conf" ]]; then
    echo "no suitable user conf for Operating System '$OS'"
    exit 1
else
    ln -s $BASEDIR/$OS.conf $USER_SCRIPT
fi
echo -e "\n\nYou may want to modify '$USER_SCRIPT'\nYou may also want to run '$BASEDIR/install_plugins.sh'"

