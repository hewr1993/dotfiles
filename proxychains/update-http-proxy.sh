#!/bin/sh
PROXY_DNS=proxy.i.brainpp.cn
PROXY_PORT=3128
PROXY_IP=$(ping -c 1 $PROXY_DNS | head -n 2 | tail -n 1 | awk '{print $4}')
cat << EOF
strict_chain
proxy_dns
remote_dns_subnet 224
tcp_read_time_out 15000
tcp_connect_time_out 8000
[ProxyList]
EOF
echo "http $PROXY_IP $PROXY_PORT"
