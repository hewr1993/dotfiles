#!/usr/bin/env python3
import os
import glob
import plistlib


if __name__ == "__main__":
    lst = list(glob.glob(os.path.expanduser("~/Library/Containers/com.tencent.xinWeChat/Data/Library/Application Support/com.tencent.xinWeChat/**/Stickers/fav.archive"), recursive=True))
    assert len(lst) > 0, "no wechat file found"
    with open(lst[0], "rb") as fin:
        mnf = plistlib.load(fin)
    for entry in mnf["$objects"]:
        if isinstance(entry, str) and entry.startswith("http://"):
            print(entry)

# vim: ts=4 sw=4 sts=4 expandtab
