#!/usr/bin/env bash
while true; do
  ssh -D 0.0.0.0:8081 -o ConnectTimeout=5 -o ConnectionAttempts=1 -o ExitOnForwardFailure=yes -o ServerAliveInterval=10 -o ServerAliveCountMax=1 -CAXY llama
  sleep 1
done
