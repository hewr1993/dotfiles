#!/usr/bin/env python3

import argparse

from huggingface_hub import snapshot_download

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(dest="repo_id", type=str)

    args = parser.parse_args()

    snapshot_download(repo_id=args.repo_id)

# vim: ts=4 sw=4 sts=4 expandtab
