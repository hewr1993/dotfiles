#!/usr/bin/env python3
import os
import json
import openai
from openai import OpenAI


def main(url, short=False, model_name="msh", n=1):
    client = OpenAI(api_key=os.environ.get("OPENAI_API_KEY", "xxx"), base_url=url)
    s = client.models.list()
    obj = s.model_dump()
    if short:
        print(obj["data"][0]["id"])
    else:
        print(json.dumps(obj))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--url", type=str, default=os.environ.get("OPENAI_API_BASE", "http://localhost:8888/v1"))
    parser.add_argument("--short", action="store_true")
    args = parser.parse_args()

    main(args.url, short=args.short)

# vim: ts=4 sw=4 sts=4 expandtab
