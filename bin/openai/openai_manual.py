#!/usr/bin/env python3
import time
import json
import urllib.parse
import requests
from loguru import logger
from sseclient import SSEClient  # pip3 install sseclient-py
from rich.live import Live
from rich.table import Table


def generate_table(lst):
    table = Table()
    table.add_column("ID")
    table.add_column("Text")
    for i, s in enumerate(lst):
        table.add_row(f"{i}", s)
    return table


def main(url, n=2, model_name="msh"):
    """
    curl 'https://vllm-hwr.app.msh.team/v1/generations' \
        -H 'content-type: application/json' \
        --data-raw $'{"model":"msh","prompt_ids":[484, 261, 19733, 1039, 621, 11724, 663, 3921, 519, 260, 262],"stream":true,"logprobs":5,"temperature":0,"top_p":1,"max_tokens":3,"n":1,"echo":true}
    """

    prompt = f"[extra_id_1]Now is {time.ctime()}. what's the minute?[extra_id_0][extra_id_2]"
    logger.info(f"prompt: {prompt}")
    target_url = urllib.parse.urljoin(url.rstrip("/") + "/", "completions")
    resp = requests.post(
        url=target_url,
        stream=True,
        headers={
            "content-type": "application/json",
            "Accept": "text/event-stream",
        },
        json={
            "model": model_name,
            "prompt": prompt,
            "stream": True,
            "logprobs": 5,
            "temperature": 0.3,
            "max_tokens": 32,
            "n": n,
            "echo": True,
        },
    )
    client = SSEClient(resp)
    ret = ["" for _ in range(n)]
    with Live(generate_table(ret)) as live:
        for event in client.events():
            if event.data == "[DONE]":
                break
            """event.data
            {
                "id": "cmpl-f942c4e3db1c48c0ab9a93f1dff490ad",
                "created": 1719630371,
                "model": "msh",
                "choices": [
                    {
                        "index": 0,
                        "text": "[extra_id_1]Now is Sat Jun 29 11:06:11 2024. what's the minute?[extra_id_0][extra_id_2]What",
                        "logprobs": {
                            "text_offset": [0],
                            "token_logprobs": [null],
                            "tokens": ["What"],
                            "top_logprobs": [null]
                        },
                        "finish_reason": null,
                        "stop_reason": null
                    }
                ],
                "usage": null
            }
            """
            obj = json.loads(event.data)
            idx = obj["choices"][0]["index"]
            delta = obj["choices"][0]["text"]
            ret[idx] += delta
            live.update(generate_table(ret))
    return ret


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--model-name", type=str, default="msh")
    parser.add_argument("--url", type=str, default="http://vllm-hwr.app.msh.team/v1")
    args = parser.parse_args()

    main(args.url, model_name=args.model_name)

# vim: ts=4 sw=4 sts=4 expandtab