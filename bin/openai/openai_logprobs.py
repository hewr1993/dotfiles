#!/usr/bin/env python3
import os
import numpy as np
import pandas as pd
from openai import OpenAI
from rich.console import Console
from rich.table import Table


class DataFramePretty(object):
    def __init__(self, df: pd.DataFrame, idx_chosens=None) -> None:
        self.data = df
        self.idx_chosens = idx_chosens or []

    def _highlight(self, df: pd.DataFrame):
        for i, row in df.iterrows():
            for j in range(len(row) // 2):
                if self.idx_chosens[i] == j:
                    df.iloc[i, j * 2] = "[b on #aa6666]" + row[f"{j}_token"] + "[/b on #aa6666]"
                    df.iloc[i, j * 2 + 1] = "[b on #aa6666]" + row[f"{j}_prob"] + "[/b on #aa6666]"

    def show(self):
        table = Table(
            title="LogProbs",
            title_style="i on dark_cyan",
            header_style="bold cyan",
        )

        df = self.data.copy()
        for col in df.columns:
            df[col] = df[col].astype("str")
            table.add_column(col)

        self._highlight(df)

        for idx in range(len(df)):
            table.add_row(*df.iloc[idx].tolist())

        console = Console()
        console.print(table)


def main(url, model_name="msh", temperature=0.3, n=1):
    messages = [
        {"role": "system", "content": "You are an assistant who is very good at translation from English to Chinese. You translate every sentence from the user in fancy ways."},
        {"role": "user", "content": "Who won the championship of men 100m in Olympics 2024?"},
    ]
    client = OpenAI(api_key=os.environ.get("OPENAI_API_KEY", "xxx"), base_url=url)
    s = client.chat.completions.create(
        model=model_name,
        messages=messages,
        max_tokens=32,
        temperature=temperature,
        n=n,
        logprobs=True,
        top_logprobs=5,
    )
    for idx, choice in enumerate(s.choices):
        print("-" * 10 + f" Choice {idx} " + "-" * 10)
        print(f"model: {model_name}")
        tokens = [c.token for c in choice.logprobs.content]
        print(f"tokens: {''.join(tokens)}")
        lpb = choice.logprobs
        data = []
        for tlp in lpb.content:
            log_probs = [l.logprob for l in tlp.top_logprobs]
            prob = np.exp(log_probs)
            prob = prob / np.sum(prob)
            row = {}
            for idx, token_prob in enumerate(prob):
                row.update(
                    {
                        f"{idx}_token": tlp.top_logprobs[idx].token,
                        f"{idx}_prob": token_prob,
                    }
                )
            data.append(row)
        df = pd.DataFrame(data)
        idx_chosen = []
        for i, row in df.iterrows():
            for j in range(len(row) // 2):
                if row[f"{j}_token"] == tokens[i]:
                    idx_chosen.append(j)
                    break
        DataFramePretty(df, idx_chosens=idx_chosen).show()

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--model-name", type=str, default="msh")
    parser.add_argument("--url", type=str, default="http://vllm-hwr.app.msh.team/v1")
    parser.add_argument("--temperature", type=float, default=0.3)
    parser.add_argument("-n", type=int, default=1)
    args = parser.parse_args()

    main(args.url, model_name=args.model_name, temperature=args.temperature, n=args.n)


# vim: ts=4 sw=4 sts=4 expandtab
