import openai
from openai import OpenAI
import os
import time
from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor, as_completed

api_base = "http://localhost:8888/v1"
#openai.api_key = "ZXZhbHRlYW06Z1BqYlhyVEc0eGI2bWRWYTFpUE9Ic3o1c0FTbXFxQmE="

model_id = "yanan-0525/pangu16b-0525"

# def list_models():
#     print("list_models")
#     model_list = openai.Model().list()
#     print("model_list: %s", model_list)

# list_models()

def main(n=1):
    client = OpenAI(api_key=os.environ.get("OPENAI_API_KEY", "xxx"), base_url=api_base)
    try:
        s = client.chat.completions.create(
            model=model_id,
            messages=[
                # {"role": "user", "content": "Jane wants to surprise her friends with a fruit salad. The recipe calls for 2 cups of diced watermelon, 3/4 cup of chopped pineapple, and 1/2 cup of sliced strawberries. However, Jane only has a 1 1/2 measuring cup. To simplify the measuring process, how many times does Jane need to use the measuring cup for the watermelon, pineapple, and strawberries, respectively? Show your steps in simplifying the problem using equivalent fractions.\n\n"},
                {"role": "user", "content": f"now is {time.time()}. introduce yourself with more than 100 words."},
            ],
            max_tokens = 128,
            n=n,
        )
        # print(s)
        # print(s['choices'][0]['message']['content'])
        print("ok")
        return s
    except Exception as e:
        if isinstance(e, openai.error.RateLimitError):
            print("RateLimit Error.")
        elif isinstance(e, openai.error.APIError):
            print("API Error.",e)
        elif isinstance(e, openai.error.InvalidRequestError):
            return "", 0
        else:
            print("found a error:", e)


def run_main(workers, count, n=1):
    tasks = []
    with ThreadPoolExecutor(workers) as pool:
        for i in range(count):
            tasks.append(pool.submit(main, n))
        for task in tqdm(as_completed(tasks), total=len(tasks)):
            print(task.result())
            # assert "choices" in task.result()
            continue


def test_request():
    run_main(1, 1)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--workers", type=int, default=1)
    parser.add_argument("--count", type=int, default=1)
    parser.add_argument("-n", type=int, default=1)
    args = parser.parse_args()

    run_main(args.workers, args.count, n=args.n)

# vim: ts=4 sw=4 sts=4 expandtab
