#!/usr/bin/env python3
import time
import os
import openai
from tqdm import tqdm
from rich.live import Live
from rich.table import Table


openai.api_key = os.environ.get("OPENAI_API_KEY", "xxx")


def generate_table(lst):
    table = Table()
    table.add_column("ID")
    table.add_column("Text")
    for i, s in enumerate(lst):
        table.add_row(f"{i}", s)
    return table


def main(n=5):
    s = openai.Completion.create(
        model="gpt-3.5-turbo",
        prompt=f"{time.ctime()} who are you? very curious",
        api_base="http://localhost:8888/v1",
        max_tokens=1024,
        temperature=0.9,
        top_p=0.7,
        n=n,
        # echo=True,
        frequency_penalty=1,
        stream=True,
    )
    ret = ["" for _ in range(n)]
    with Live(generate_table(ret)) as live:
        for obj in s:
            idx = obj["choices"][0]["index"]
            delta = obj["choices"][0]["text"]
            ret[idx] += delta
            live.update(generate_table(ret))
    return ret


if __name__ == "__main__":
    main()

# vim: ts=4 sw=4 sts=4 expandtab
