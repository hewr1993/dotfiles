#!/usr/bin/env python3
import os
import openai
from openai import OpenAI
import json
from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor, as_completed



def main(url, model):
    client = OpenAI(api_key=os.environ.get("OPENAI_API_KEY", "xxx"), base_url=url)
    s = client.chat.completions.create(
        model=model,
        messages=[
            {"role": "system", "content": "You are an assistant who is very good at translation from English to Chinese. You translate every sentence from the user."},
            {"role": "user", "content": "Who won the world series in 2020?"},
        ],
    )
    # print(s)
    return s.model_dump()


def run_main(url, model, workers, count):
    tasks = []
    with ThreadPoolExecutor(workers) as pool:
        for i in range(count):
            tasks.append(pool.submit(main, url, model))
        for task in tqdm(as_completed(tasks), total=len(tasks)):
            print(json.dumps(task.result(), ensure_ascii=False, indent=2))


def test_request():
    run_main(1, 1)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--url", type=str, default=os.environ.get("OPENAI_API_BASE", "http://localhost:8888/v1"))
    parser.add_argument("--model", type=str, default="gpt-3.5-turbo")
    parser.add_argument("--workers", type=int, default=1)
    parser.add_argument("--count", type=int, default=1)
    args = parser.parse_args()

    run_main(args.url, args.model, args.workers, args.count)

# vim: ts=4 sw=4 sts=4 expandtab
