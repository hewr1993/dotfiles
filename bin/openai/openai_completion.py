#!/usr/bin/env python3
import os
import openai
from openai import OpenAI
import json
from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor, as_completed




def main(url, model):
    client = OpenAI(api_key=os.environ.get("OPENAI_API_KEY", "xxx"), base_url=url)
    s = client.completions.create(
        model=model,
        # model="gpt-3.5-turbo",
        prompt="who are you",
        # api_base=url,
        max_tokens=256,
        temperature=0.9,
        top_p=0.7,
        n=1,
        #echo=True,
        #frequency_penalty=1,
    )
    return s


def run_main(workers, count, *args):
    tasks = []
    with ThreadPoolExecutor(workers) as pool:
        for i in range(count):
            tasks.append(pool.submit(main, *args))
        for task in tqdm(as_completed(tasks), total=len(tasks)):
            # print(json.dumps(task.result(), ensure_ascii=False, indent=2))
            print(task.result())


def test_request():
    run_main(1, 1)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--url", type=str, default=os.environ.get("OPENAI_API_BASE", "http://127.0.0.1:8888/v1"))
    parser.add_argument("--model", type=str, default="msh")
    parser.add_argument("--workers", type=int, default=1)
    parser.add_argument("--count", type=int, default=1)
    args = parser.parse_args()

    run_main(args.workers, args.count, args.url, args.model)

# vim: ts=4 sw=4 sts=4 expandtab
