#!/usr/bin/env python3
import time
import os
import argparse
import openai
from openai import OpenAI
from tqdm import tqdm
from rich.live import Live
from rich.table import Table


openai.api_key = os.environ.get("OPENAI_API_KEY", "xxx")

# prompt = """<|im_start|>system
# You are DiscoLM, a helpful assistant.
# <|im_end|>
# <|im_start|>user
# Please tell me possible reasons to call a research collective "Disco Research"<|im_end|>
# <|im_start|>assistant"""

# prompt = """<|im_start|>system
# You are DiscoLM, a helpful assistant.
# <|im_end|>
# <|im_start|>user
# who are you?<|im_end|>
# <|im_start|>assistant"""

# prompt = f"Now is {time.ctime()}. Hello, my name is"

# prompt = "[extra_id_1]Hello, my name is kimi.[extra_id_0][extra_id_2]"

prompt = f"[extra_id_1]Now is {time.ctime()}. what's the minute?[extra_id_0][extra_id_2]"

# prompt = "[extra_id_0]晚上睡不着怎么办？？？[extra_id_1]"


def generate_table(lst):
    table = Table()
    table.add_column("ID")
    table.add_column("Text")
    for i, s in enumerate(lst):
        table.add_row(f"{i}", s)
    return table


def main(url, n=1, model_name="msh", prompt_file=None, max_new_tokens=32):
    global prompt
    if prompt_file is not None:
        with open(prompt_file) as fin:
            prompt = fin.read()
    print(prompt)
    client = OpenAI(api_key=os.environ.get("OPENAI_API_KEY", "xxx"), base_url=url)
    s = client.completions.create(
        # model="gpt-3.5-turbo",
        model=model_name,
        prompt=prompt,
        max_tokens=max_new_tokens,
        temperature=0.3,
        # top_p=0.7,
        n=n,
        # echo=True,
        # frequency_penalty=1,
        stream=True,
    )
    ret = ["" for _ in range(n)]
    with Live(generate_table(ret)) as live:
        for obj in s:
            # print(obj)
            # idx = obj["choices"][0]["index"]
            # delta = obj["choices"][0]["text"]
            idx = obj.choices[0].index
            delta = obj.choices[0].text
            ret[idx] += delta
            live.update(generate_table(ret))
    return ret


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--model-name", type=str, default="msh")
    parser.add_argument("--prompt-file", type=str)
    parser.add_argument("--max-new-tokens", type=int, default=32)
    parser.add_argument("--url", type=str, default="http://localhost:8888/v1")
    args = parser.parse_args()

    main(
        args.url,
        model_name=args.model_name,
        prompt_file=args.prompt_file,
        max_new_tokens=args.max_new_tokens,
    )

# vim: ts=4 sw=4 sts=4 expandtab
