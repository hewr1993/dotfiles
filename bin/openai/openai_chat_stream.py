#!/usr/bin/env python3
import os
import openai
from openai import OpenAI
from tqdm import tqdm
from rich.live import Live
from rich.table import Table



def generate_table(lst):
    table = Table()
    table.add_column("ID")
    table.add_column("Text")
    for i, s in enumerate(lst):
        table.add_row(f"{i}", s)
    return table


def main(url, model_name="msh", n=1, max_tokens=32, temperature=0.3):
    client = OpenAI(api_key=os.environ.get("OPENAI_API_KEY", "xxx"), base_url=url)
    with open("/mnt/moonfs/hwr-ksyun/tests/texts/32K.from_yanan.txt") as fin:
        text = fin.read()
    s = client.chat.completions.create(
        model=model_name,
        messages=[
            {"role": "system", "content": "You are an assistant who is very good at translation from English to Chinese. You answer every sentence from the user in fancy ways and give as many details as possible."},
            {"role": "user", "content": text},
        ],
        max_tokens=max_tokens,
        temperature=temperature,
        n=n,
        logprobs=True,
        top_logprobs=5,
        # echo=True,
        # frequency_penalty=1,
        stream=True,
    )
    ret = ["" for _ in range(n)]
    with Live(generate_table(ret)) as live:
        for obj in s:
            idx = obj.choices[0].index
            delta = obj.choices[0].delta
            if getattr(delta, "content", None):
                ret[idx] += delta.content
                live.update(generate_table(ret))
    return ret


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--model-name", type=str, default="msh")
    parser.add_argument("--url", type=str, default=os.environ.get("OPENAI_API_BASE", "http://vllm-hwr.app.msh.team/v1"))
    parser.add_argument("-n", type=int, default=1)
    parser.add_argument("--max-tokens", type=int, default=32)
    parser.add_argument("--temperature", type=float, default=0.3)
    args = parser.parse_args()

    main(args.url, model_name=args.model_name, n=args.n, max_tokens=args.max_tokens, temperature=args.temperature)

# vim: ts=4 sw=4 sts=4 expandtab
