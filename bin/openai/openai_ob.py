#!/usr/bin/env python3
import click
import os
import time
from openai import OpenAI
from transformers import AutoTokenizer
from loguru import logger


def generate_prompt(fn, tknz, nr_tokens):
    with open(fn) as fin:
        text = fin.read()
    tokens = tknz.encode(text)
    ret = tokens
    while len(ret) < nr_tokens:
        ret += tokens
    return tknz.decode(ret[:nr_tokens])


@click.command()
@click.option("--api-base", type=str, default="http://localhost:8888/v1")
@click.option("-n", type=int, default=1)
@click.option("--max-tokens", type=int, default=16)
@click.option("--model", type=str, default="msh")
@click.option("--prompt-tokens", type=int, default=30)
@click.option("--tokenizer", type=str, default="/mnt/moonfs/hwr-ksyun/models/msh-7B/sft-jblrl-iter2236-tf")
@click.option("--corpora", type=str, default="/mnt/moonfs/hwr-ksyun/tests/texts/token-8k.from-00224.txt")
def main(
    api_base: str,
    n: int,
    max_tokens: int,
    model: str,
    prompt_tokens: int,
    tokenizer: str,
    corpora: str,
):
    tknz = AutoTokenizer.from_pretrained(tokenizer, trust_remote_code=True)
    prompt = generate_prompt(corpora, tknz, prompt_tokens)

    client = OpenAI(api_key=os.environ.get("OPENAI_API_KEY", "xxx"), base_url=api_base)
    s = client.completions.create(
        model=model,
        prompt=prompt,
        max_tokens=max_tokens,
        temperature=0.9,
        n=n,
        stream=True,
    )
    times = [[time.time()] for _ in range(n)]
    for obj in s:
        idx = obj.choices[0].index
        is_prefill = len(times[idx]) == 1
        times[idx].append(time.time())
        dur = times[idx][-1] - times[idx][-2]
        if is_prefill:
            tok_s = prompt_tokens / dur
        else:
            tok_s = 1 / dur
        logger.debug(f"[{idx}] {len(times[idx]) - 1: 4d} tokens {dur * 1000:.3f} ms {tok_s:.2f} tok/s")
    min_time = max([lst[1] if len(lst) > 1 else 0 for lst in times])
    max_time = min([lst[-1] if len(lst) > 1 else 0 for lst in times])
    total_tokens = sum([1 for lst in times for t in lst[1:] if min_time <= t <= max_time]) - n
    total_tok_s = total_tokens / (max_time - min_time + 1e-6)
    avg_tok_s = total_tok_s / n
    logger.info(f"total {total_tok_s:.2f} tok/s\tavg {avg_tok_s:.2f} tok/s")

if __name__ == "__main__":
    main()

# vim: ts=4 sw=4 sts=4 expandtab
