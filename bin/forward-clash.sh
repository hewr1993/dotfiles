#!/usr/bin/env bash
while true; do
  ssh -R 0.0.0.0:7890:0.0.0.0:7890 -o ExitOnForwardFailure=yes -o ServerAliveInterval=10 -o ServerAliveCountMax=1 -CAXY compile.hwr.megvii-aic.ws@hh-d.brainpp.cn
  sleep 1
done
